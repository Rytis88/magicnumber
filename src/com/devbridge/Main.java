package com.devbridge;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		int number = scan.nextInt();
		int magicNum = 0;
		int x;
		int y;
		int tempNum;
		String tempNumString;
		
		System.out.println("Input a number: ");
		int len = String.valueOf(number).length();
		
		for(int i=2;i<=len;i++){ 
			
			tempNum = number * i;
			
			for(int j=1; j<=len; j++){ 
				
				x = tempNum % 10;
				y = tempNum / 10;
				tempNumString = x +""+ y;
			
				if (String.valueOf(number).equals(tempNumString) ) {
					magicNum++; 
					break;
				}
		
			tempNum = Integer.valueOf(tempNumString);
			}
		}
		if (len-1 == magicNum) {
			System.out.println("It's magic!");
		}	
	}		
}